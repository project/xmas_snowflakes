(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.MyModuleBehavior = {
    attach: function (context, settings) {
      if (context !== document) {
        // Exit on ajax forms.
        return;
      }
      var sf = new Snowflakes({
        color: drupalSettings.xmas_snowflakes.color,
        count: drupalSettings.xmas_snowflakes.count,
        minOpacity: drupalSettings.xmas_snowflakes.minOpacity,
        maxOpacity: drupalSettings.xmas_snowflakes.maxOpacity,
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
