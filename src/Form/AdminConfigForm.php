<?php

namespace Drupal\xmas_snowflakes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provide config form for xmas snowflakes.
 */
class AdminConfigForm extends ConfigFormBase {

  const CONFIG_NAME = 'xmas_snowflakes.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xmas_snowflakes_admin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(static::CONFIG_NAME);
    $form += [
      'visibility' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Snowflake conditions'),
        'visibility_hidden' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Hidden completely'),
          '#description' => $this->t('Killswitch: Don\'t display snowflakes at all.<br /><i>Hint: This will override all other visibility settings</i>'),
          '#default_value' => $config->get('visibility_hidden') ?? FALSE,
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);
    $config->set('visibility_hidden', $form_state->getValue('visibility_hidden') ? TRUE : FALSE);
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

}
