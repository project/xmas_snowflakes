<?php

namespace Drupal\xmas_snowflakes\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xmas_snowflakes\Form\AdminConfigForm;

/**
 * Provides Snowflakes Block.
 *
 * @Block(
 *   id = "xmas_snowflakes",
 *   admin_label = @Translation("Xmas Snowflakes"),
 * )
 */
class SnowflakesBlock extends BlockBase {

  /**
   * Build snowflakes block.
   */
  public function build() : array {
    $config = \Drupal::config(AdminConfigForm::CONFIG_NAME);
    $build = [];
    $block_config = $this->getConfiguration();
    $cache_tags = $config->getCacheTags();
    if (!$config->get('visibility_hidden')) {
      $build['#attached']['library'][] = 'xmas_snowflakes/snowflakes';
      $build['#attached']['drupalSettings']['xmas_snowflakes'] = [
        'color' => $block_config['snowflakes_color'] ?? '#70deff',
        'count' => $block_config['snowflakes_count'] ?? 50,
        'minOpacity' => $block_config['snowflakes_opacity_min'] ?? 0.2,
        'maxOpacity' => $block_config['snowflakes_opacity_max'] ?? 0.7,
      ];
    }
    $build['#cache']['tags'] = $cache_tags;
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $form += [
      'snowflakes_color' => [
        '#type' => 'textfield',
        '#title' => $this->t('Color'),
        '#description' => $this->t('Use hex value like <i>#70deff</i>'),
        '#default_value' => $config['snowflakes_color'] ?? '#70deff',
      ],
      'snowflakes_count' => [
        '#type' => 'number',
        '#min' => 1,
        '#max' => 1000,
        '#step' => 1,
        '#title' => $this->t('Count'),
        '#description' => $this->t('Configures how many snowflakes can be visible at max.'),
        '#default_value' => $config['snowflakes_count'] ?? 50,
      ],
      'snowflakes_opacity_min' => [
        '#type' => 'number',
        '#min' => 0,
        '#max' => 1,
        '#step' => 0.1,
        '#title' => $this->t('Min opacity'),
        '#description' => $this->t('Configures the minimum opacity for a snowflake.'),
        '#default_value' => $config['snowflakes_opacity_min'] ?? 0.2,
      ],
      'snowflakes_opacity_max' => [
        '#type' => 'number',
        '#min' => 0,
        '#max' => 1,
        '#step' => 0.1,
        '#title' => $this->t('Max opacity'),
        '#description' => $this->t('Configures the maximum opacity for a snowflake.'),
        '#default_value' => $config['snowflakes_opacity_max'] ?? 0.7,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->setConfigurationValue('snowflakes_color', $form_state->getValue('snowflakes_color'));
    $this->setConfigurationValue('snowflakes_count', $form_state->getValue('snowflakes_count'));
    $this->setConfigurationValue('snowflakes_opacity_min', $form_state->getValue('snowflakes_opacity_min'));
    $this->setConfigurationValue('snowflakes_opacity_max', $form_state->getValue('snowflakes_opacity_max'));
  }

}
